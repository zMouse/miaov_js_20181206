# Vue 脚手架

> 虽然我们可以像前面学习 Vue 基础的时候一样，在需要的页面中引入对应的 js 文件，但是实际项目开发中还有许多其他事情需要去处理，比如打包、压缩、在低版本的浏览器中支持模块化、ES6+新特性，还有 Vue 提供的单文件组件开发模式，为了能够使我们更加方便的使用以上功能进行项目开发，Vue 官方为我们提供了项目开发工具，就像盖大楼所需要使用的脚手架一样，它为我们 Vue 项目开发所需的基础支持，这个工具就是 [Vue CLI](https://cli.vuejs.org/zh/)



### 安装 Vue-CLI

- 官网：https://cli.vuejs.org/zh/

- CLI：*command-line interface*，命令行接口，表示该程序是通过命令行的方式来使用的，对应的是 GUI：*Graphical user interface*，图形用户界面
- Vue CLI 是基于 Node.js 开发的，所以在安装 Vue CLI 之前需要安装 Node.js
  - 注意：最新 v3.3 版本 Vue CLI 需求 Node.js 版本为 v8.9+



##### 安装

```shell
npm install -g @vue/cli
```

通过上述方式安装 Vue CLI 成功以后，会提供一个全局的 vue 命令，后续就是通过这个 vue 命令来帮助我们构建 Vue 应用，以及测试、打包等……

命令帮助：

```shell
vue -h
// or
vue --help
```



##### 查看当前 Vue CLI 版本

```shell
vue -V
// or
vue --version
```



### 创建项目

我们可以通过 Vue CLI 提供的子命令 `create` 来创建项目

```shell
vue create [项目名称]
```

该命令会帮助我们创建项目对应的目录和项目所需的文件和依赖包，该过程是交互式的，也就是在创建过程会提供一些选项让我们根据实际情况进行选择

##### 选择预设配置

![1551571998044](1.png)

- 使用官方提供的默认预设配置
- 手动选择相关配置

##### 选择需要安装的特性（功能）

![1551572146877](2.png)

- **Babel**：选择是否使用 Babel，这个推荐安装，因为这样我们就可以在项目中使用 ES6+、JSX 等语法，Babel 会帮助我们转换它们为浏览器支持的 JS
- **TypeScript**：选择是否使用 TypeScript，如果你熟悉或者项目需要使用 TS，那么可以安装它
- **Progressive Web App （PWA） Support**：Progressive Web App（PWA）Support，渐进式WEB应用，[参考](https://developer.mozilla.org/zh-CN/docs/Web/Progressive_web_apps)，[介绍](https://developer.mozilla.org/zh-CN/docs/Web/Progressive_web_apps/Introduction)

- **Router**：安装使用 Router
- **Vuex**：安装使用 Vuex
- **CSS Pre-processors**：安装使用 CSS Pre-processors，CSS 的预处理工具，比如项目中我们使用 LESS、SASS、Stylus 那么就可以选择这一项，然后再次根据提示选择对应的需要的 CSS 预处理工具
- **Linter / Formatter**：Linter / Formatter，代码风格检测工具，帮助我们在开发过程中检测代码错误，同时也可以规范代码格式，提高代码质量和团队合作开发效率，比较有名的工具就是 ESLint，选择这个选项以后，后续就会提示我们选择具体的检测工具了，比如 ESLint
- **Unit Testing**：是否安装使用单元测试框架
- **E2E Testing**：是否安装端到端测试框架

##### 保存配置到项目

![1551573260319](3.png)

选择设置完项目相关配置以后，会提示我们把配置保存到什么位置

- 保存到单独的配置文件中
- 保存合并到 package.json 文件中

##### 保存预设配置，以供复用

![1551573539280](4.png)

是否把当前的预设配置保存起来，方便以后的项目复用该预设配置

##### 创建并安装项目

最后一步，根据以上的预设配置创建项目，并配置安装所需要的依赖

![1551573687951](5.png)

安装成功完成以后，我们应该就能看到这样的信息

##### 通过 UI 方式创建项目

Vue CLI 还提供了一个 UI 图形界面的方式来创建项目，我们只需要使用：

```shell
vue ui
```

Vue CLI 会构建一个本地 web 服务器，并提供了一个 web 方式的安装界面，安装的流程与命令行方式的一模一样，不过需要注意的，如果需要后续通过这个来管理项目，比如安装扩展、安装插件，那么就需要保证这个本地 web 服务器的处于运行状态，否则需要使用 vue ui 命令再次启动



### 项目目录结构

##### 根目录

![1551588822728](6.png)

- public：外链资源存放目录
- src：源码存放目录，这也是我们项目开发中的主要目录
- tests：测试脚本存放目录

##### src 目录

![1551589548554](7.png)

- assets
  - 作为 `webpack` 模块引用资源存放目录，assets 与 public 目录的区别是，assets 是通过 webpack 引入的资源存放目录，它会在 webpack 编译过程中处理，而 public 是通过浏览器访问过程中通过 HTTP 读取访问
- components
  - 业务组件存放目录
- views
  - 视图组件存放目录
  - 业务组件 与 视图组件 的区别：视图组件与路由绑定，也就是该组件是通过某个 URL 来进行访问，所以也可以叫 路由组件、页面组件，业务组件 更像我们平时所说的组件，也就是页面中某个具有独立功能的模块，比如：菜单、导航、轮播图、选项卡等
- main.js
  - 应用入口文件
  - 该文件是 vue 项目入口文件，该文件加载了一些模块，并实例化 Vue 实例对象，该实例对象 el 挂载的元素在 public/index.html 文件中
- App.vue
  - 这个以 vue 作为后缀的文件是 Vue 提供的一种单文件组件，该文件对我们开发 Vue 组件提供更方便的支持，该文件会被 webpack 编译，具体后续详细解释
- router.js
  - 路由模块
- store.js
  - vuex 模块，应用数据存储仓库



### 启动

在项目开发过程中，我们通过 Vue CLI 提供的内置 web 服务来启动本地环境

```shell
npm run serve
// or
npx vue-cli-service serve [options]
```

##### 启动选项

- --open
  - 在服务器启动时打开浏览器
- --copy 在服务器启动时将 URL 复制到剪切版
- --mode 指定环境模式 (默认值：development)
- --host 指定 host (默认值：0.0.0.0)
- --port 指定 port (默认值：8080)
- --https 使用 https (默认值：false)



### vue 单文件组件

参考：https://vue-loader.vuejs.org/zh/

vue 单文件组件模式，把一个独立组件的结构、样式以及行为封装到了一个单一的文件中，更有利于组件的开发和维护

vue 单文件组件以 .vue 为后缀，一个 vue 单文件组件包含结构如下：

![单文件组件](./vue-component-with-preprocessors.png)

- \<template>：用来编写组件模板
  - 可以通过 `lang` 属性指定模板所使用的语言（引擎），比如：`jade`，默认为 `html`
- \<script>：编写组件脚本
  - 可以通过 `lang` 属性指定模板所使用的语言（引擎），比如：`ts`，默认为 `javascript`
- \<style>：编写组件样式
  - 可以通过 `lang` 属性指定模板所使用的语言（引擎），比如：`stylus`，默认为 `css`
  - 当 `<style>` 标签有 `scoped` 属性时，它的 CSS 只作用于当前组件中的元素，通过这个可以让组件的样式具有独立性

### 打包构建

这里需要注意的是，项目中的大部分文件只是开发过程中使用，并不需要运行在生产环境（正式环境）中，这个时候我们就需要把生产环境所需要的代码文件进行打包构建，然后把构建好的最终文件传输到正式环境服务器中

```shell
npm run build
// or
npx vue-cli-service build [options]
```

##### 构建选项

- --mode
  - 指定环境模式 (默认值：production)
- --dest
  - 指定输出目录 (默认值：dist)
- --modern
  - 面向现代浏览器不带自动回退地构建应用
- --target
  - app | lib | wc | wc-async (默认值：app)
- --name
  - 库或 Web Components 模式下的名字 (默认值：package.json 中的 "name" 字段或入口文件名)
- --no-clean
  - 在构建项目之前不清除目标目录 --report 生成 report.html 以帮助分析包内容
- --report-json
  - 生成 report.json 以帮助分析包内容
- --watch
  - 监听文件变化



### 配置文件

Vue 还提供了一套单独的配置文件来帮助我们进行应用开发和构建，该配置文件是可选的，我们只需要在项目根目录下创建一个 `vue.config.js`，写上对应配置，当我们 启动 或 打包构建 项目的时候，该配置文件会被自动加载

该文件导出了一个包含了选项的对象，格式如下：

```js
module.exports = {
	// 选项... 
}
```

##### 选项

- publicPath
  - 设置应用的基础 URL，应用中的 URL 将都基于此进行设置
  - 默认：'/'

- outputDir
  - 设置 build 后的构建文件存放目录
  - 目标目录在构建之前会被清除 (构建时传入 --no-clean 可关闭该行为)
  - 默认：'dist'
- assetsDir
  - 放置生成的静态资源 (js、css、img、fonts) 的 (相对于 `outputDir` 的) 目录
  - 默认：''
- indexPath
  - 指定生成的 `index.html` 的输出路径 (相对于 `outputDir`)。也可以是一个绝对路径
  - 默认：'index.html'
- pages
  - 构建多页面应用时候的设置，具体参考：[多页面配置](https://cli.vuejs.org/zh/config/#pages)

- devServer
  - 配置本地内置服务器
    - host：主机
    - port：端口
    - ……
    - proxy，这个具体后续详解



