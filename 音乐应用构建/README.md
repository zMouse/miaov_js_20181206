# 音乐应用构建

经过前面基础的学习，我们现在可以使用它们来搭建一个完整的小应用，来熟悉基于 Vue + Koa 的前后端开发

### 应用介绍

当前这个应用提取了主要几个页面和功能，这样就能够更快速、清晰的了解开发过程，实际应用只是根据具体需求在此基础上进行扩展，过程原理基本相同。

##### 页面功能

[模板文件](https://gitlab.com/zMouse/miaov_music)

- 首页
  - 以分页的方式展示所有歌手
- 歌手详情页
  - 展示某个具体歌手的详情信息，包括歌手基本资料，该歌手的热门歌曲与专辑
- 专辑列表页面
  - 以分页的方式展示某个歌手所有的专辑
- 歌曲列表页面
  - 以分页的方式展示某个歌手所有的歌曲
- 注册登录页面
  - 提供用户注册与登录，为后续的评论与搜索的功能提供基础
- 评论
  - 为某些需要评论的模块（歌手、专辑、歌曲等）提供评论的功能
  - 该功能基于用户模块



### 应用构建

##### 初始化应用

我们通过 Vue CLI 来构建应用

```shell
vue create music-vue
```

当前项目中 Vue CLI 配置选择如下（仅供参考）：

特性：

![1552118823768](1552118823768.png)

路由模式：

![1552118890902](1552118890902.png)

测试框架：

![1552118925484](1552118925484.png)

e2e测试框架：

![1552118952943](1552118952943.png)

配置保存目标

![1552118997729](1552118997729.png)

是否保存当前配置，供以后复用

![1552119343372](1552119343372.png)



应用初始化完成以后，结构目录如下，我们可以把其中不需要的（下图红框处）删除掉

![1552119953895](1552119953895.png)



##### 路由配置

根据前面应用介绍中说的页面，然后进行应用路由配置，打开路由文件 `./src/router.js`，修改如下：

```javascript
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'Home',
            component: () => import(/* webpackChunkName: "Home" */ './views/Home.vue')
        },
        {
            path: '/singer',
            name: 'Singer',
            component: () => import(/* webpackChunkName: "Singer" */ './views/Singer.vue')
        },
        {
            path: '/songs',
            name: 'Songs',
            component: () => import(/* webpackChunkName: "Song" */ './views/Songs.vue')
        },
        {
            path: '/albums',
            name: 'Albums',
            component: () => import(/* webpackChunkName: "Albums" */ './views/Albums.vue')
        },
        {
            path: '/register',
            name: 'Register',
            component: () => import(/* webpackChunkName: "Register" */ './views/Register.vue')
        },
        {
            path: '/login',
            name: 'Login',
            component: () => import(/* webpackChunkName: "Login" */ './views/Login.vue')
        }
    ]
});
```

##### 路由懒加载

在上面的代码中，我们可以看到这么一段代码：

```javascript
component: () => import(/* webpackChunkName: "Home" */ './views/Home.vue')
```

这个是 vue-router 中的路由懒加载，[参考](https://router.vuejs.org/zh/guide/advanced/lazy-loading.html#%E6%8A%8A%E7%BB%84%E4%BB%B6%E6%8C%89%E7%BB%84%E5%88%86%E5%9D%97)

- 能把不同路由对应的组件分割成不同的代码块，避免打包一个目标文件的体积过大
- 当路由被访问的时候才加载对应组件，达到按需加载的目的

下面列出的是每个路由对应的页面组件

- 首页

  ```javascript
  {
      path: '/',
      name: 'Home',
      component: () => import(/* webpackChunkName: "Home" */ './views/Home.vue')
  }
  ```

![1552117675139](1552117675139.png)

- 歌手详情页

```javascript
{
    path: '/singer',
    name: 'Singer',
    component: () => import(/* webpackChunkName: "Singer" */ './views/Singer.vue')
}
```

![1552121761083](1552121761083.png)

- 专辑列表页面

```javascript
{
    path: '/albums',
    name: 'Albums',
    component: () => import(/* webpackChunkName: "Albums" */ './views/Albums.vue')
}
```

![1552121856298](1552121856298.png)

- 歌曲列表页面

```javascript
{
    path: '/songs',
    name: 'Songs',
    component: () => import(/* webpackChunkName: "Song" */ './views/Songs.vue')
}
```

![1552121919232](1552121919232.png)

- 注册页面

```javascript
{
    path: '/register',
    name: 'Register',
    component: () => import(/* webpackChunkName: "Register" */ './views/Register.vue')
}
```

![1552121997058](1552121997058.png)

- 登录页面

```javascript
{
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "Login" */ './views/Login.vue')
}
```

![1552122074487](1552122074487.png)



\>>> [进入第二部分](./PART_2.md)