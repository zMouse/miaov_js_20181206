# 音乐应用构建

### 接口开发

下面我们就可以正式来开发对应的接口了

- 根据指定页数查询歌手列表
- 查询指定歌手详细信息
- 分页查询指定歌手的专辑
- 分页查询指定歌手的歌曲
- 用户注册
- 用户登陆
- 分页查询评论
- 评论提交评论提交



### 根据指定页数查询歌手列表

```javascript
/**
 * 根据指定页数查询歌手列表
 * page表示第几页，(\\d+)表示只有数字才匹配该路由，也可以不传入，不传入会在singers方法中具体处理
 * /singers => 满足
 * /singers/1 => 满足
 * /singers/a => 不满足
 */
router.get('/singers/:page(\\d+)?', singerMod.singers);
```

```javascript
singers: async ctx => {

    // 每页显示多少条
    let perpage = 10;
    // 接收url后面的数字，表示当前第几页，默认为1
    let page = ctx.params.page || 1;
    // 后续根据数据 总条数 / prepage 计算出来的中页数
    let pages = 0;

    // 获取所有数据的总记录数
    // 这个查询会返回一个数组，数组的第0项是查询出来的对应字段数据
    // 在字段数据中有一个count的字段，我们把他给解构出来
    let [[{count: singersCount}]] = await ctx.db.query(
        'SELECT COUNT(id) as count FROM `singers`'
    );

    // 总页数
    pages = Math.ceil(singersCount / perpage);

    page = Math.max( page, 1 );
    page = Math.min( page, pages);

    // 查询数据库的时候，数据的偏移值
    let start = (page - 1) * perpage;
    // 查询指定页数的歌手数据
    let [singers] = await ctx.db.query(
        "SELECT * FROM `singers` LIMIT ? OFFSET ?",
        [perpage, start]
    );
    // console.log(singers);

    ctx.body = {
        code: 0,
        message: '',
        data: {
            perpage,
            page,
            singersCount,
            pages,
            singers: singers.map( singer => {
                return {
                    ...singer,
                    image: `/public/images/singers/${singer.mid}.jpg`
                }
            } )
        }
    };
}
```

#### 关于歌手对应的图片相关的说明

图片资源存放在后端，所以我们在后端创建一个文件夹用来存放图片

- /public/photos/singers 目录下存放所有歌手的图片，图片名称与歌手的 `mid` 对应：singer.mid + '.jpg'
- /public/photos/albums 目录下存放所有专辑的图片，图片名称与专辑的 `mid` 对应： albums.mid + '.jpg'

前面我们讲过这种静态资源，我们可以直接使用 `koa-static-cache` 模块来完成

```javascript
// app.js
...
const KoaStaticCache = require('koa-static-cache');
...

async function run() {
  const app = new Koa();
  app.use( KoaStaticCache( path.join(__dirname, 'public'), {
    prefix: '/public',
    dynamic: true
  } ) );
}
run();
```

通过上面的代码，我们就可以通过类似 http://localhost:9090/public/photos/singers/000aHmbL2aPXWH.jpg，来访问图片了。

为了避免在后期反复的拼接路径，我们可以把路径配置到配置文件中：

```javascript
const configs = {
  development: {
    ...,
    assetsPath: {
      singer: '/public/photos/singers/',
      album: '/public/photos/albums/',
    }
  }
}
```



#### 分页

使用路由变化来更新分页数据请求

##### 动态路由

```javascript
// 路由内
{
    path: '/:page(\\d+)?',
    name: 'Home',
    component: () => import(/* webpackChunkName: "Home" */ './views/Home.vue')
}

// 组件内
this.$route.params.page
```

##### 编程式导航：使用 js 来导航路由

```javascript
this.$router.push();
```

##### 组件内 路由守卫

```javascript
beforeRouteUpdate(to, from, next) {
    next();
    let page = this.$route.params.page || 1;
    this.getSingers( {page} );
}
```



### 前端调用

为了方便数据请求的管理和复用，我们把数据请求有关的具体业务代码从组件中分离出去，放到我们的 vuex 的 store 中

- state 中存放共享数据
- action 中实现异步数据更新
- mutation 中同步更新 state



\>>> [进入第八部分](./PART_8.md)