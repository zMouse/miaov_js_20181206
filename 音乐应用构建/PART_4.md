# 音乐应用构建

### 创建初始化项目

创建后端项目目录 `music-backend`

通过命令行（终端）进入目录，然后 npm 初始化

```shell
npm init -y
```



安装一些必要的依赖，这里直接贴出安装后的列表（package.json），具体依赖的用处后面用到再细说

```json
{
  "name": "music-backend",
  "version": "1.0.0",
  "description": "",
  "main": "app.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "keygrip": "^1.0.3",
    "koa": "^2.7.0",
    "koa-bodyparser": "^4.2.1",
    "koa-router": "^7.4.0",
    "koa-session": "^5.10.1",
    "koa-static-cache": "^5.1.2",
    "mysql2": "^1.6.5"
  }
}
```



### 入口文件

创建项目入口（主）文件，`app.js`

```javascript
const Koa = require('koa');

const app = new Koa();

app.listen(9090);
```

因为后续有很多的一些可变的数据，比如这里的端口 9090，可能在不同的环境下会有不同的配置，所以，我们把应用中的一些可变值都提取到一个配置文件中，进行引入，我们在项目根目录下创建一个 configs.js 文件

```javascript
// configs.js
const configs = {
    // 开发环境下使用的配置
    development: {
        host: '0.0.0.0',
        port: 9090
    },
    // 生产（线上）环境下使用的配置
    product: {
        host: '0.0.0.0',
        port: 9090
    }
}

// process.env.NODE_ENV 是 Node中获取当前机器的环境变量的方式，具体看下图，这样就在不同的环境下导出不同的配置
module.exports = configs[ process.env.NODE_ENV || 'development' ];
```

![1552579328404](1552579328404.png)

我们可以给当前电脑设置环境变量，名称为 NODE_ENV 值就是 development 或者 product 等，这样当代码运行的时候去读取这个值，然后根据不同的值得到不同的结果，这种方式很常用也是适用，因为这样就可以修改机器的这个值来让代码在不同的模式下运行起来了

```javascript
// app.js

const Koa = require('koa');
// 导入配置文件
const config = require('./configs.js');

const app = new Koa();

// 根据导入的配置开启不同的端口
app.listen(config.port, config.host, () => {
    console.log(`服务成功开启：http://${config.host}:${config.port}`);
});
```

### 定义 api 路由

```javascript
// app.js

const Koa = require('koa');
// 导入配置文件
const config = require('./configs.js');
// 导入路由中间件
const KoaRouter = require('koa-router');

const app = new Koa();

// 创建路由实例对象
const router = new KoaRouter();

// 后续这里会有很多路由，代码会越来越多，单个文件体积也会越来越大，且不利于后期的维护，所以我们把路由函数拆分到不同的文件中去
router.get('/', async ctx => {
    // ...
});

// 挂载路由对象到 app 中间件中
app.use( router.routes() );

// 根据导入的配置开启不同的端口
app.listen(config.port, config.host, () => {
    console.log(`服务成功开启：http://${config.host}:${config.port}`);
});
```

从 `app.js` 文件中剥离路由

```javascript
// app.js 
// 新增，把singer有关的路由方法拆分到 mods（没有就新建一个） 目录下的 singer.js 中
const singerMod = require('./mods/singer');

/**
 * 根据指定页数查询歌手列表
 */
router.get('/singers', singerMod.singers);
```

```javascript
// ./mods/singer.js

module.exports = {
    async singers() {
        
    }
}
```

下面我们就根据上面的规则定义所需要的 api 路由，结果如下：

```javascript
const Koa = require('koa');
// 导入配置文件
const config = require('./configs.js');
// 导入路由中间件
const KoaRouter = require('koa-router');
// 路由文件
// 歌手相关接口
const singerMod = require('./mods/singer');
// 专辑相关接口
const albumMod = require('./mods/album');
// 歌曲相关接口
const songMod = require('./mods/song');
// 用户相关接口
const userMod = require('./mods/user');
// 评论相关接口
const commentMod = require('./mods/comment');

const app = new Koa();

// 创建路由实例对象
const router = new KoaRouter();

/**
 * 根据指定页数查询歌手列表
 * page表示第几页，(\\d+)表示只有数字才匹配该路由，也可以不传入，不传入会在singers方法中具体处理
 * /singers => 满足
 * /singers/1 => 满足
 * /singers/a => 不满足
 */
router.get('/singers/:page(\\d+)?', singerMod.singers);

/**
 * 查询指定歌手详细信息
 * id 表示具体歌手的id，必传
 */
router.get('/singer/:id(\\d+)', singerMod.singer);

/**
 * 分页查询指定歌手的专辑
 * singerId 表示具体歌手id
 * page 表示第几页
 */
router.get('/albums/:singerId(\\d+)/:page(\\d+)?', albumMod.albums);

/**
 * 分页查询指定歌手的歌曲
 * singerId 表示具体歌手id
 * page 表示第几页
 */
router.get('/songs/:singerId(\\d+)/:page(\\d+)?', songMod.songs);

/**
 * 用户注册
 */
router.post('/register', KoaBodyparser(), userMod.register);

/**
 * 用户登陆
 */
router.post('/login', KoaBodyparser(), userMod.login);

/**
 * 分页查询评论
 * singerId 表示具体歌手id
 * page 表示第几页
 */
router.get('/comments/:singerId(\\d+)/:page(\\d+)?', commentMod.comments);

/**
 * 评论提交
 * singerId 表示具体歌手id
 */
router.post('/comments/:singerId(\\d+)', commentMod.post);

// 挂载路由对象到 app 中间件中
app.use( router.routes() );

// 根据导入的配置开启不同的端口
app.listen(config.port, config.host, () => {
    console.log(`服务成功开启：http://${config.host}:${config.port}`);
});
```

具体的路由处理文件

```javascript
// ./mods/singer.js

module.exports = {
    async singers(ctx) {
        // 这里我们定义接口的规则，返回一个json
        ctx.body = {
            // 返回的错误码，0表示没有错误，其它值具体定义
            code: 0,
            // 返回的错误描述
            message: '',
            // 返回的数据，具体根据实际需要返回的值决定
            data: {}
        }
    }
    async singer(ctx) {
        // ...
    }
}
```

其它的 `./mods` 下的文件就不一一列出了，下面我们构建好我们需要的数据库，导入数据，然后连接数据库，编写具体的业务代码



\>>> [进入第五部分](./PART_5.md)