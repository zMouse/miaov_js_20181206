# 音乐应用构建

### 功能组件编写

前面说过，除了路由组件，在一个页面中可能会存在一些复用的部分（非整个页面），那么我们也可以把这部分内容作为组件提取出来

##### 简单组件

把通用头部和尾部作为功能组件提取出去（前面讲过，路由组件放在 <u>`./src/views/`</u> 目录下，功能组件放在 `./src/components/` 目录下）。

随后，我们在 <u>`App.vue`</u> 中进行 <u>`import`</u> 并放置在页面适当的位置中。

> 当我们希望在 <u>`template`</u> 中使用外部某个外部组件的时候，首先确保该组件已经被 <u>`import`</u>，其次，我们还需要在当前 `template` 所在的组件对象的 <u>`components`</u> 属性中进行声明，这样才可以使用该组件。

```vue
<template>
    <div id="app">

        <music-header></music-header>

        <div class="main">
            <router-view></router-view>
        </div>

        <music-footer></music-footer>
    </div>
</template>

<script>

    import MusicHeader from '@/components/MusicHeader.vue';
    import MusicFooter from '@/components/MusicFooter.vue';

    export default {
        name: "App",
        // 在 template 中使用的外部组件需要在 components 选项中进行声明
        components: {
            MusicHeader,
            MusicFooter
        }
    }

</script>
```



### 功能组件

大家可以看到，有些组件仅仅只是一个结构样式的提取，但是有的组件还涉及到了行为和数据的变化，比如分页，在不同的页面中会多次复用分页这个组件，但是分页的数量以及点击分页的后续行为却又是不一样的，所以这个时候，我们就需要对分页组件进行更深入的处理。

我们先来看一下分页的基本结构样式：

```vue
<template>
    <div class="pages">

        <a href="/page-1" class="prev"> &lt; </a>

        <a href="/n+1" class="current"><strong>1</strong></a>
        <a href="/n+1">2</a>
        <a href="/n+1">3</a>
        <a href="/n+1">4</a>

        <a href="/page+1" class="next"> &gt; </a>

    </div>
</template>

<script>
    import '@/assets/css/components/page.css';

    export default {
        name: 'Page'
    }
</script>
```

首先，编写这样的一个通用组件，我们需要关注这么几个点：

- `props`：也就是 `属性`，是组件对外提供的数据接口
- `events`：也就是 `事件`，是组件对外提供的交互接口
- `slots`：也就是 `插槽`，是组件对外提供的内容（子级）接口

以上这些都是组件与外界交互的方式，当然一个组件还包含了内部的一些特性，比如：

- `data`：组件内部私有数据
- `methods`：组件内部行为

下面我们就以分页组件为例，以后在编写任意组件的之前，都需要先去设计组件原型，也就是上面提到的这几个点

**属性**

| 名称    | 说明           | 类型   | 可选值 | 默认值 | v-model |
| ------- | -------------- | ------ | ------ | ------ | ------- |
| total   | 数据总条目数   | number | —      | —      |         |
| prepage | 每页显示条目数 | number | —      | 10     |         |
| page    | 当前页         | number | —      | 1      | 是      |

**事件**

| 名称       | 说明                               | 回调参数                       | v-model |
| ---------- | ---------------------------------- | ------------------------------ | ------- |
| change     | \<data>currentPage 改变时会触发    | 改变后的 \<data>currentPage 值 | 是      |
| prev-click | 用户点击上一页按钮改变当前页后触发 | 改变后的 \<data>currentPage 值 |         |
| next-click | 用户点击下一页按钮改变当前页后触发 | 改变后的 \<data>currentPage 值 |         |

**data**

| 名称  | 说明     | 类型   | 默认值 | v-model |
| ----- | -------- | ------ | ------ | ------- |
| pages | 总页码数 | number | 1      |         |

- 根据 <u>`total`</u> 和 <u>`prepage`</u> 的值计算得到总页码数 <u>`pages`</u> 并显示
- 高亮当前页
- 给 <u>`上一页`</u> 、<u>`下一页`</u> 以及 <u>`每一页`</u> 添加 <u>`click`</u> 事件，并在事件触发的时候修改 <u>`page`</u> 的值
- 处理 <u>`v-model`</u> 逻辑：触发 <u>`v-model`</u> 指定的事件



### 带插槽组件

有的是否一个组件所包含的子级内容结构是可变化的，比如 <u>`Songs.vue`</u> 和 <u>`Albums.vue`</u> 组件中

```vue
<div class="mod_part">
    <div class="header">
        <h2>{{title}}</h2>
    </div>

    <!--被包含的可变子级内容结构-->
</div>
```

这个时候，我们构建一个 <u>`ModPart.vue`</u> 组件，该组件的可变子级内容就需要通过 <u>`slot`</u> 来完成了

```vue
<template>
    <div class="mod_part">
        <div class="header">
            <h2>{{title}}</h2>
        </div>

        <slot></slot>
    </div>
</template>

<script>
    export default {
        name: 'ModPart',
        props: {
            title: {
                type: String
            }
        }
    }
</script>
```

然后在 <u>`Songs.vue`</u> 和 <u>`Albums.vue`</u> 中使用它

<u>`Songs.vue`</u>

```vue
<template>
    <div>

        <mod-part title="XXX - 歌曲列表">
            <div class="song_list">
                <ul class="songlist_header">
                    <li class="songlist_number">&nbsp;</li>
                    <li class="songlist_name">歌曲</li>
                    <li class="songlist_album">专辑</li>
                    <li class="songlist_time">时长</li>
                </ul>
                <ul class="songlist_list">

                    <li>
                        <div class="songlist_number">1</div>
                        <div class="songlist_name">
                            <a href="">song.name</a>

                            <div class="tools">
                                <i class="tools_item tools_play"></i>
                                <i class="tools_item tools_add"></i>
                                <i class="tools_item tools_down"></i>
                                <i class="tools_item tools_share"></i>
                            </div>
                        </div>
                        <div class="songlist_album">
                            <a href="">光年之外</a>
                        </div>
                        <div class="songlist_time">
                            <span>song.interval</span>
                        </div>
                    </li>

                    <li>
                        <div class="songlist_number">1</div>
                        <div class="songlist_name">
                            <a href="">song.name</a>

                            <div class="tools">
                                <i class="tools_item tools_play"></i>
                                <i class="tools_item tools_add"></i>
                                <i class="tools_item tools_down"></i>
                                <i class="tools_item tools_share"></i>
                            </div>
                        </div>
                        <div class="songlist_album">
                            <a href="">光年之外</a>
                        </div>
                        <div class="songlist_time">
                            <span>song.interval</span>
                        </div>
                    </li>

                    <li>
                        <div class="songlist_number">1</div>
                        <div class="songlist_name">
                            <a href="">song.name</a>

                            <div class="tools">
                                <i class="tools_item tools_play"></i>
                                <i class="tools_item tools_add"></i>
                                <i class="tools_item tools_down"></i>
                                <i class="tools_item tools_share"></i>
                            </div>
                        </div>
                        <div class="songlist_album">
                            <a href="">光年之外</a>
                        </div>
                        <div class="songlist_time">
                            <span>song.interval</span>
                        </div>
                    </li>

                    <li>
                        <div class="songlist_number">1</div>
                        <div class="songlist_name">
                            <a href="">song.name</a>

                            <div class="tools">
                                <i class="tools_item tools_play"></i>
                                <i class="tools_item tools_add"></i>
                                <i class="tools_item tools_down"></i>
                                <i class="tools_item tools_share"></i>
                            </div>
                        </div>
                        <div class="songlist_album">
                            <a href="">光年之外</a>
                        </div>
                        <div class="songlist_time">
                            <span>song.interval</span>
                        </div>
                    </li>

                </ul>
            </div>
        </mod-part>

        <page></page>

    </div>
</template>

<script>
    import '@/assets/css/song.css';

    import ModPart from '@/components/ModPart';
    import Page from '@/components/Page';

    export default {
        name: 'Songs',
        components: {
            ModPart,
            Page
        }
    }
</script>
```

<u>`Albums.vue`</u>

```vue
<template>
    <div>

        <mod-part title="XXX - 专辑列表">
            <ul class="album_list">

                <li class="playlist_item">
                    <div class="playlist_item_box">
                        <div class="playlist_item_cover">
                            <a href="">
                                <img src="" />
                            </a>
                        </div>
                        <h4 class="playlist_item_title">
                            <a href="">album.name</a>
                        </h4>
                        <div class="playlist_item_date">
                            <span>album.pubTime</span>
                        </div>
                    </div>
                </li>

                <li class="playlist_item">
                    <div class="playlist_item_box">
                        <div class="playlist_item_cover">
                            <a href="">
                                <img src="" />
                            </a>
                        </div>
                        <h4 class="playlist_item_title">
                            <a href="">album.name</a>
                        </h4>
                        <div class="playlist_item_date">
                            <span>album.pubTime</span>
                        </div>
                    </div>
                </li>

                <li class="playlist_item">
                    <div class="playlist_item_box">
                        <div class="playlist_item_cover">
                            <a href="">
                                <img src="" />
                            </a>
                        </div>
                        <h4 class="playlist_item_title">
                            <a href="">album.name</a>
                        </h4>
                        <div class="playlist_item_date">
                            <span>album.pubTime</span>
                        </div>
                    </div>
                </li>

                <li class="playlist_item">
                    <div class="playlist_item_box">
                        <div class="playlist_item_cover">
                            <a href="">
                                <img src="" />
                            </a>
                        </div>
                        <h4 class="playlist_item_title">
                            <a href="">album.name</a>
                        </h4>
                        <div class="playlist_item_date">
                            <span>album.pubTime</span>
                        </div>
                    </div>
                </li>

                <li class="playlist_item">
                    <div class="playlist_item_box">
                        <div class="playlist_item_cover">
                            <a href="">
                                <img src="" />
                            </a>
                        </div>
                        <h4 class="playlist_item_title">
                            <a href="">{{album.name</a>
                        </h4>
                        <div class="playlist_item_date">
                            <span>album.pubTime</span>
                        </div>
                    </div>
                </li>

            </ul>
        </mod-part>

        <page></page>

    </div>
</template>

<script>
    import '@/assets/css/album.css';

    import Page from '@/components/Page';
    import ModPart from '@/components/ModPart';

    export default {
        name: 'Albums',
        components: {
            ModPart,
            Page
        }
    }
</script>
```

当然，如果可以，你可以对页面中的一些结构进行更加细的组件化，这个取决于实际应用情况和你个人的决定



到此，我们就使用 vue 完成了不带数据行为交互的组件化构建



\>>> [进入第四部分](./PART_4.md)