# 音乐应用构建

### 数据库创建 & 数据导入

这里我准备了一个数据库构建 与 数据导入的程序，该程序在提供的源码 `project-database` 目录中

##### 开启数据库服务

首先，需要搭建一个 `mysql` 服务器，并启动

##### 运行数据库构建到数据导入脚本

命令进入 `project-database` 目录，运行导入命令（首次使用需要先安装依赖）

```shell
// 进入 project-database 目录
cd project-database
// 安装依赖
npm install
// 运行脚本，需要联入网络
node index
```

数据库构建 & 数据导入 成功以后就可以在后端接口程序中使用了



### 连接数据库

这里我们需要用到前面安装到 `mysql2` 模块，然后使用的 `mysql2` 的 `promise`，因为要用到异步 `async await`，所以下面的代码需要稍微做下改变，要使用一个异步函数包装一下：

```javascript
const Koa = require('koa');
// 导入配置文件
const config = require('./configs.js');
// 导入路由中间件
const KoaRouter = require('koa-router');
// 数据库模块
const mysql2 = require('mysql2/promise');
// 路由文件
// 歌手相关接口
const singerMod = require('./mods/singer');
// 专辑相关接口
const albumMod = require('./mods/album');
// 歌曲相关接口
const songMod = require('./mods/song');
// 用户相关接口
const userMod = require('./mods/user');
// 评论相关接口
const commentMod = require('./mods/comment');

// 把有一步的操作放到这个异步函数中
async function run() {
  const app = new Koa();
  
  // 连接数据库, config.db 在配置文件中定义，这里就用到了await
  app.context.db = await mysql2.createConnection(config.db);

  // 创建路由实例对象
  const router = new KoaRouter();

  /**
   * 根据指定页数查询歌手列表
   * page表示第几页，(\\d+)表示只有数字才匹配该路由，也可以不传入，不传入会在singers方法中具体处理
   * /singers => 满足
   * /singers/1 => 满足
   * /singers/a => 不满足
   */
  router.get('/singers/:page(\\d+)?', singerMod.singers);

  /**
   * 查询指定歌手详细信息
   * id 表示具体歌手的id，必传
   */
  router.get('/singer/:id(\\d+)', singerMod.singer);

  /**
   * 分页查询指定歌手的专辑
   * singerId 表示具体歌手id
   * page 表示第几页
   */
  router.get('/albums/:singerId(\\d+)/:page(\\d+)?', albumMod.albums);

  /**
   * 分页查询指定歌手的歌曲
   * singerId 表示具体歌手id
   * page 表示第几页
   */
  router.get('/songs/:singerId(\\d+)/:page(\\d+)?', songMod.songs);

  /**
   * 用户注册
   */
  router.post('/register', KoaBodyparser(), userMod.register);

  /**
   * 用户登陆
   */
  router.post('/login', KoaBodyparser(), userMod.login);

  /**
   * 分页查询评论
   * singerId 表示具体歌手id
   * page 表示第几页
   */
  router.get('/comments/:singerId(\\d+)/:page(\\d+)?', commentMod.comments);

  /**
   * 评论提交
   * singerId 表示具体歌手id
   */
  router.post('/comments/:singerId(\\d+)', commentMod.post);

  // 挂载路由对象到 app 中间件中
  app.use( router.routes() );

  // 根据导入的配置开启不同的端口
  app.listen(config.port, config.host, () => {
      console.log(`服务成功开启：http://${config.host}:${config.port}`);
  });
}

run();
```

以上代码，数据库连接在 `23行` 

配置文件：

```javascript
const configs = {
    development: {
        host: '127.0.0.1',
        port: 9090,
        db: {
            host: '127.0.0.1',
            port: 3306,
            user: 'root',
            password: '',
            database: 'miaov_music_development'
        }
    }
};

module.exports = configs[ process.env.mode || 'development' ];
```

到此为止，关于数据库相关方面的已经准备齐全了



\>>> [进入第六部分](./PART_6.md)