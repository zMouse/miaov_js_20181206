# 音乐应用构建

### vue 项目与后端接口请求

有了后端接口的支持，那么我们就可以在 vue 项目中使用这些接口了，我们可以使用原生的 ajax 来实现请求，也可以使用一些封装框架，这里我们选择使用一个基于原生 XMLHttpRequest 对象来封装的框架：axios

#### 安装

```shell
npm i axios
```

#### 官网地址

https://www.npmjs.com/package/axios

#### 基本使用

##### .get(url)

##### .post(url, data)

##### axios(options)

​	options 的具体配置参考文档

​	注意：

​		- params：queryString

​		- data：正文数据

axios 是一个基于 promise 的封装，所以我们可以通过 then 方法，或者 async await 来进行后续处理

axios 的promise 返回值是一个 response 对象，该对象下包含了请求相关的一些数据信息，比如状态码，响应内容等……

跟多的使用细节，我们后续需要用到的时候再去了解



### 跨域

我们会发现在 vue 中发送的请求出现了跨域问题，因为开发过程中我们是在 vue 临时构建的一个 webserver 环境下，而服务器是在另外一个 webserver 下的，那么这个时候，我们就需要解决开发过程中出现的这个跨域问题。

我们可以使用代理的方式来解决这个问题，其实我们 vue 脚手架帮助我们开启了一个 webserver，而这个 webserver 内置了一个代理服务，我们只需要做一些简单的配置，就可以解决这个跨域问题了

#### vue.config.js 文件

https://cli.vuejs.org/zh/

前面我们提到过一个 vue.config.js 文件，在这个文件中就有一项关于这个内置 webserver 的配置：devServer，在这个配置下面，有一个 proxy 的选项，就是用来配置代理的，简单配置如下：

```javascript
module.exports = {
  devServer: {
    proxy: 'http://localhost:4000'
  }
}
```

如果我们需要代理的信息比较多，那么可以使用如下类似配置：

```javascript
module.exports = {
  devServer: {
    proxy: {
      '/api': {
        target: '<url>',
        ws: true,
        changeOrigin: true
      },
      '/foo': {
        target: '<other_url>'
      }
    }
  }
}
```



\>>> [进入第七部分](./PART_7.md)