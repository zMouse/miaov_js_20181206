# 音乐应用构建

### 项目基础构建

配置好对应的路由以后，接下来就需要对对应的页面组件进行编码了，这里准备好了对应的静态文件，可以从 https://gitlab.com/zMouse/miaov_music.git 克隆项目的源码，在 template 目录下就有全部的静态资源：

![1552122840621](1552122840621.png)

![1552122906578](1552122906578.png)

**html文件目录**

![1552122926100](1552122926100.png)

**CSS样式目录**

![1552122953748](1552122953748.png)



准备好了对应的静态文件以后，就接下来



##### vue 静态资源

把静态 css 和 images 目录文件拷贝到项目 `./src/assets` 对应的目录下：

![1552123322554](1552123322554.png)

##### 项目入口文件

入口文件这里暂时没有什么需要增加太多的处理，我们可以在入口这里导入全局通用 css 文件

```javascript
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 这里载入全局通用的 base.css 文件
import '@/assets/css/base.css';

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
```

`注`：@ 在这里表示一个路径，指向 `./src`， 在解析编译的时候被处理。



##### App.vue

这个文件是项目的根（顶层）组件，经过我们对静态文件的观察发现，整个应用都有统一的头部和尾部，所以我们把头部和尾部（应用通用的结构）放在这里，然后在页面正文部分，也就是 `<div class="main"></div>` 中放置一个 `<router-view></router-view>` ，根据不同的路由 URL 加载不同的页面组件（比如，首页、歌手详情页，注册登录等）到这里展示。

```vue
<template>
  <div id="app">
    <header class="clear">
      <h1>
        <a href="/"></a>
      </h1>
      <div class="header_user">
        <div class="hide user">
          <a href>username</a>
          <span>|</span>
          <a href="/logout">退出</a>
        </div>

        <div class="register_login">
          <a href="/login">登录</a>
          <span>|</span>
          <a href="/register">注册</a>
        </div>
      </div>
    </header>

    // 不同的 url 对应的 views 组件将会显示在这里，比如 Home.vue
    <router-view/>

    <footer>
      <div class="footer_copyright">
        <p>
          <a href="#">关于我们</a> |
          <a href="#">About Us</a> |
          <a href="#">服务条款</a> |
          <a href="#">用户服务协议</a> |
          <a href="#">隐私政策</a> |
          <a href="#">权利声明</a> |
          <a href="#">广告服务</a>
        </p>

        <p>Copyright &copy; 0000 - 2019 Music.
          <a href="#">All Rights Reserved.</a>
        </p>
        <p>xxxx
          <a href="#">版权所有</a>
          <a href="#">腾讯网络文化经营许可证</a>
        </p>
      </div>
    </footer>
  </div>
</template>
```

##### Home.vue……

```vue
<template>
  <div class="main">
    <div class="singer_list">
      <ul class="clear">
        <li class="singer_list_item">
          <div class="singer_list_item_box">
            <a href="/detail/">
              <img src="">
            </a>
            <h3>
              <a href="/detail/">singer.name</a>
            </h3>
          </div>
        </li>
        <li class="singer_list_item">
          <div class="singer_list_item_box">
            <a href="/detail/">
              <img src="">
            </a>
            <h3>
              <a href="/detail/">singer.name</a>
            </h3>
          </div>
        </li>
        <li class="singer_list_item">
          <div class="singer_list_item_box">
            <a href="/detail/">
              <img src="">
            </a>
            <h3>
              <a href="/detail/">singer.name</a>
            </h3>
          </div>
        </li>
        <li class="singer_list_item">
          <div class="singer_list_item_box">
            <a href="/detail/">
              <img src="">
            </a>
            <h3>
              <a href="/detail/">singer.name</a>
            </h3>
          </div>
        </li>
        <li class="singer_list_item">
          <div class="singer_list_item_box">
            <a href="/detail/">
              <img src="">
            </a>
            <h3>
              <a href="/detail/">singer.name</a>
            </h3>
          </div>
        </li>
        <li class="singer_list_item">
          <div class="singer_list_item_box">
            <a href="/detail/">
              <img src="">
            </a>
            <h3>
              <a href="/detail/">singer.name</a>
            </h3>
          </div>
        </li>
        <li class="singer_list_item">
          <div class="singer_list_item_box">
            <a href="/detail/">
              <img src="">
            </a>
            <h3>
              <a href="/detail/">singer.name</a>
            </h3>
          </div>
        </li>
        <li class="singer_list_item">
          <div class="singer_list_item_box">
            <a href="/detail/">
              <img src="">
            </a>
            <h3>
              <a href="/detail/">singer.name</a>
            </h3>
          </div>
        </li>
        <li class="singer_list_item">
          <div class="singer_list_item_box">
            <a href="/detail/">
              <img src="">
            </a>
            <h3>
              <a href="/detail/">singer.name</a>
            </h3>
          </div>
        </li>
        <li class="singer_list_item">
          <div class="singer_list_item_box">
            <a href="/detail/">
              <img src="">
            </a>
            <h3>
              <a href="/detail/">singer.name</a>
            </h3>
          </div>
        </li>
      </ul>
    </div>

    <div class="pages">
      <a href="/page-1" class="prev">&lt;</a>
      
      <a href="/n+1" class="current">
        <strong>1</strong>
      </a>
      <a href="/n+1">2</a>
      <a href="/n+1">3</a>
      <a href="/n+1">4</a>
      
      <a href="/page+1" class="next">&gt;</a>
    </div>
  </div>
</template>

<script>
import '@/assets/css/index.css';

export default {
    name: 'Home'
}
</script>
```

按照 `router.js` 中的定义，完成其它几个路由组件的编写……



\>>> [进入第三部分](./PART_3.md)