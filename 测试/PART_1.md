# 测试

通常，为了保证程序能够正确在生产环境中运行，我们需要在上线之前对程序进行一些必要的测试，根据测试的过程与目的的不同，又分成不同类型的测试：

- 单元测试
- e2e测试
- 测试覆盖率

等等。如果这些测试都靠人力进行，那就会为此付出极大的成本，而且系统越大成本也就越大。所以就诞生了各种各样的自动化测试工具，也就是我们经常提到的各种测试框架，下面我们就来具体说说这些测试框架：



### 单元测试

该类型测试是站在程序员的角度进行的测试，目的是为了测试系统中最小可测试的单元(比如函数，接口)进行检查和测试。

无论是测试函数还是其它，测试无非就是检测它们各种不同情景下得到的结果是否符合预期，如果是则测试通过，否则就是没有通过测试。

##### 开始

我们就从最简单的测试开始。现在，我们新建一个文件 `1.js` 并创建 了一个函数，该函数接受一个字符串参数，作用是把传入的字符串变成驼峰形式，'background-color' => 'backgroundColor'，具体代码如下：

```javascript
function toCamelCase(str) {
  return str.replace(/\-(\w)/g, ($0, $1) => {
    return $1.toLocalUpperCase();
  });
}
```

下面我要对我们写的这个方法进行一些必要的测试，来验证该方法在使用的时候是否得到我们的预期结果。

首先，我们就来具体说下我们将要使用的单元测试框架：mocha

### mocha

官网：<https://mochajs.org/>

Mocha 是一个拥有丰富特性 JavaScript 测试框架，可以用来对前端 JavaScript 和后端 Node.js 代码进行单元测试。

##### 安装

```shell
npm i -g mocha	//安装在全局
// 或者
npm i -S mocha //安装在当前本地项目
```

mocha 是一个单元测试框架，用来执行我们的测试脚本，我们还要安装一个断言库，所谓的断言，使用编程术语，使用一些特定方法来预测（验证）某个行为与结果的正确性，通常表示为一个 boolean 结果。

断言库也有很多，有 Node.js 自带的 assert 断言模块，也有一些功能更加强大的，如：should.js、chai……，这里我们使用：chai

```shell
npm i -S chai
```

##### 编写测试脚本

下面我们就可以创建一个测试脚本文件：1.test.js

```javascript
let m1 = require('./1.js');
let expect = require('chai').expect;

describe('测试 m1 模块', () => {
  it('toCamelCase 方法测试场景一', () => {
    expect( m1.toCamelCase('background-color') ).to.equal( 'backgroundColor' );
  });
  it('toCamelCase 方法测试场景二', () => {
    expect( m1.toCamelCase('background--color') ).to.equal( 'backgroundColor' );
  });
});
```

每个具体的测试都需要包含至少一个 describe 和 若干 it

describe：用来表示一组测试单元

it：用来表示一个测试用例，是测试的最小单元

expect：这个是断言库中的其中一个方法，传入我们要断言的函数调用场景代码的返回值

to.equal：预测的期望结果，equal 就是相等的意思，那么也就是如果 `expect` 的结果与 `equal` 的值相等，那么就是断言成功，否则就是失败。

下面就让我们来运行 mocha 来进行测试

```shell
// 如果是全局安装的话，node/npm 相关使用可以参考对应视频和文章进行了解
mocha 1.test.js
```

运行上述命令以后，我们应该能看到下图中类似的信息：

![image-20190325173009166](image-20190325173009166.png)

我们可以通过上面的文字就能看出，我们一共测试了两个单元，一个测试通过了，一个测试失败了。

